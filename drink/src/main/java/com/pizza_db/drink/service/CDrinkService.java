package com.pizza_db.drink.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import com.pizza_db.drink.model.CDrink;
import com.pizza_db.drink.repository.IDrinkRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class CDrinkService {
    @Autowired
	IDrinkRepository pDrinkRepository;

    public ArrayList<CDrink> getDrinkList() {
        ArrayList<CDrink> listDrink = new ArrayList<>();
        pDrinkRepository.findAll().forEach(listDrink::add);
        return listDrink;
    }
    
    public CDrink getDrinkById(long id) {
        Optional<CDrink> drinkData = pDrinkRepository.findById(id);
        if (drinkData.isPresent()) {
			return drinkData.get();
		} else {
			return null;
		}
    }

    public CDrink createDrink(CDrink pDrink) {
        Optional<CDrink> drinkData = pDrinkRepository.findById(pDrink.getId());
        if(drinkData.isPresent()) {
            return null;
        } else {
            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);            
            return pDrink;
        }
    }

    public CDrink updateDrinkById(long id, CDrink pDrink) {
        Optional<CDrink> drinkData = pDrinkRepository.findById(id);
		if (drinkData.isPresent()) {
			CDrink drink = drinkData.get();
			drink.setMaNuocUong(pDrink.getMaNuocUong());
            drink.setTenNuocUong(pDrink.getTenNuocUong());
			drink.setDonGia(pDrink.getDonGia());
            drink.setGhiChu(pDrink.getGhiChu());
			drink.setNgayCapNhat(new Date());
            return drink;
		} else {
            return null;
        }
    }

}
